import React from "react";
import {render} from "react-dom";

import Home from './components/home';

import "normalize.css";
import "./styles/index.scss";

render(<Home/>, document.getElementById("root"));

module?.hot?.accept();
