const path = require("path");
const webpack = require("webpack");
const { merge } = require("webpack-merge");
const baseConfig = require("./base.config");

module.exports = merge(baseConfig, {
    mode: "development",

    devtool: "eval-source-map",

    devServer: {
        hot: true,
        port: "3000",
        inline: true,
        compress: true,
        clientLogLevel: "silent",
        contentBase: path.join(__dirname, "../src"),
        historyApiFallback: true,
    },

    plugins: [new webpack.HotModuleReplacementPlugin({})],
});
